[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4686969.svg)](https://doi.org/10.5281/zenodo.4686969)

# Energy demand scenarios in buidlings until the year 2050 - ambitious policy scenario


## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation

This datasets includes scenario results for the development of energy demand for space heating and domestic hot water on NUTS0 level for EU 28.
The results are based on the model [Invert/EE-Lab](https://www.invert.at)[1]. The main modelling work was carried out within the Set-Nav project [Set-Nav project](http://www.set-nav.eu/). 

The ambitious policy scenario assumes that moderate, more ambitious policies are implemented. In particular, we assume that in general decision makers comply with regulatory instruments like building codes. National differences in the policy intensity continue to exist.

Details on results and methodologies can be found in [1][2][3].

The scenario data will be used in the Hotmaps toolbox to estimate potential developments of energy demand in EU28 in order to compare results on local developments with overal efficiency and emission mitigation targets on country and EU28 level.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps: D5.2 Heating & Cooling outlook until 2050](http://www.hotmaps-project.eu/library/) 




### References
[1] [Invert EE-Lab model](https://www.invert.at)
[2] [Set-Nav project - Case study report on energy demand in buildings](http://www.set-nav.eu/sites/default/files/common_files/deliverables/wp5/D5.3%20Case%20study%20report%20on%20energy%20in%20buildings.pdf)
[3] [Hotmaps: D5.2 Heating & Cooling outlook until 2050, EU-28, 2018] (http://www.hotmaps-project.eu/library/)


## How to cite


Lukas Kranzl, Michael Hartner, Andreas Müller, Gustav Resch, Sara Fritz (TUW), Andreas Müller (e‐think),Tobias Fleiter, Andrea Herbst, Matthias Rehfeldt, Pia Manzi (Fraunhofer ISI,)
Alyona Zubaryeva (EURAC), reviewed by Jakob Rager (CREM): Hotmaps Project, D5.2 Heating & Cooling outlook until 2050, EU-28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/library/) 


## Authors
Michael Hartner <sup>*</sup>,
Lukas Kranzl <sup>*</sup>,
Sebastian Forthuber <sup>*</sup>,
Sara Fritz <sup>*</sup>,
Andreas Müller <sup>*</sup>,

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/)
Institute of Energy Systems and Electrical Drives
Gusshausstrasse 27-29/370
1040 Wien



## License
This work is licensed under a CC BY (Creative Commons By Attribution)

Copyright © 2016-2018: Michael Hartner, Lukas Kranzl, Sebastian Forthuber, Sara Fritz, Andreas Müller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.